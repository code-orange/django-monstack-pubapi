from django.contrib import admin

from django.conf import settings
from django.urls import include, path
from django.urls import re_path

urlpatterns = [
    re_path(r"", include("user_sessions.urls", "user_sessions")),
    path("admin/", admin.site.urls),
    re_path(r"^watchman/", include("watchman.urls")),
    path(
        "monitoring/api/",
        include("django_monstack_apps_api.django_monstack_apps_api.urls"),
    ),
    path(
        "monitoring/api/",
        include("django_monstack_hosts_api.django_monstack_hosts_api.urls"),
    ),
    path(
        "monitoring/api/",
        include("django_monstack_services_api.django_monstack_services_api.urls"),
    ),
    path(
        "monitoring/api/",
        include("django_monstack_hardware_api.django_monstack_hardware_api.urls"),
    ),
    # Next Generation API
    path(
        "monitoring-ng/api/",
        include("django_monstack_api_ng.django_monstack_api_ng.urls"),
    ),
]

if settings.DEBUG:
    import debug_toolbar

    urlpatterns = [
        path("__debug__/", include(debug_toolbar.urls)),
    ] + urlpatterns
